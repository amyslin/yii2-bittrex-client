<?php
namespace artdevision\bittrex;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Client extends HttpClient {

	protected $apiKey;

	protected $_defaultOptions = [
		'base_uri' => 'https://bittrex.com/api/',
	];

	protected $apiSecret;

	protected $apiVersion;

	const PUBLIC_MARKETS = "getmarkets";
	const PUBLIC_CURRENCIES = "getcurrencies";
	const PUBLIC_TICKER = "getticker";
	const PUBLIC_MARKETSUMMARIES = "getmarketsummaries";
	const PUBLIC_MARKETSUMMARIY = "getmarketsummary";
	const PUBLIC_ORDERBOOK = "getorderbook";
	const PUBLIC_MARKETHISTORY = "getmarkethistory";

	const MARKET_BUYLIMIT = "buylimit";
	const MARKET_SELLLIMIT = "selllimit";
	const MARKET_CANCEL = "cancel";
	const MARKET_OPENORDERS = "getopenorders";

	const ACCOUNT_BALANCES = "getbalances";
	const ACCOUNT_BALANCE = "getbalance";
	const ACCOUNT_DEPOSITADDRESS = "getdepositaddress";
	const ACCOUNT_WITHDRAW = "withdraw";
	const ACCOUNT_ORDER = "getorder";
	const ACCOUNT_ORDERHISTORY = "getorderhistory";
	const ACCOUNT_WITHDRAWALHISTORY = "getwithdrawalhistory";
	const ACCOUNT_DEPOSITHISTORY = "getdeposithistory";

	public function __construct($apiKey, $apiSecret, $options = [], $version = 'v1.1') {
		$config = \yii\helpers\ArrayHelper::merge($this->_defaultOptions, $options);
		$this->apiKey = $apiKey;
		$this->apiSecret = $apiSecret;
		$this->apiVersion = $version;

		$handler = new \GuzzleHttp\Handler\CurlHandler([
				'handle_factory' => new CurlFactory(3),
		]);
		$stack = \GuzzleHttp\HandlerStack::create($handler);

		$config['handler'] = $stack;


		parent::__construct($config);
	}
	/**
	 *
	 * @param string $method
	 * @param array $params
	 * @return ResponseInterface
	 */
	public function getPublic($method, $params = []) {
		return $this->request('GET', $this->apiVersion. '/public/'.$method, [
				RequestOptions::QUERY => $params,
		]);
	}

	/**
	 *
	 * @param string $method
	 * @param array $params
	 * @return ResponseInterface
	 */
	public function getMarket($method, $params = []) {
		$params = \yii\helpers\ArrayHelper::merge(['apikey' => $this->apiKey, 'nonce' => time()], $params);
		$url = $this->apiVersion. '/market/'.$method . '?' . http_build_query($params);
		return $this->request('GET', $url, [
// 				RequestOptions::QUERY => $params,
				RequestOptions::HEADERS => [
					'apisign' => hash_hmac('sha512', $this->_defaultOptions['base_uri'] . $url, $this->apiSecret),
				],
		]);
	}

	/**
	 *
	 * @param string $method
	 * @param array $params
	 * @return ResponseInterface
	 */
	public function getAccount($method, $params = []) {
		$params = \yii\helpers\ArrayHelper::merge(['apikey' => $this->apiKey, 'nonce' => time()], $params);
		$url = $this->apiVersion. '/account/'.$method . '?' . http_build_query($params);
		return $this->request('GET', $url, [
// 				RequestOptions::QUERY => $params,
				RequestOptions::HEADERS => [
					'apisign' => hash_hmac('sha512', $this->_defaultOptions['base_uri'] . $url, $this->apiSecret),
				],
		]);
	}

}