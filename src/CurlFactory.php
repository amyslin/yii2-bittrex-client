<?php
namespace artdevision\bittrex;

use GuzzleHttp\Handler\EasyHandle;
use GuzzleHttp\Handler\CurlFactory as GuzzleCurlFactory;

class CurlFactory extends GuzzleCurlFactory {
	public function release(EasyHandle $easy) {
		if ($easy->response) {
			$easy->response->curl_info = curl_getinfo($easy->handle);
		}
		return parent::release($easy);
	}
}